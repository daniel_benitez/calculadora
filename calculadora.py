# Calculadora basica en Python

class Calculadora:
    
    #Función constructora de la clase
    def _int_(self):
        
        self.operando_1=0
        self.operando_2=0
        self.resultado=0
    
    def sumar(self):
        
        self.resultado=self.operando_1+self.operando_2
    
    def restar(self):
        
        self.resultado=self.operando_1-self.operando_2
    
    def multiplicar(self):
        
        self.resultado=self.operando_1*self.operando_2
    
    def dividir(self):
        if self.operando_2==0:
            self.resultado==0
        else:
            self.resultado==self.operando_1/self.operando_2
    
    def modulo(self):
        
        self.resultado=self.operando_1%self.operando_2