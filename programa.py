#Importar la clase

from calculadora import Calculadora


class Interactuador:

    def menu_principal():
        print("Bienvenido a esta Calculadora. ")
        print("1 . Sumar")
        print("1 . Restar")
        print("1 . Multiplicar")
        print("1 . Dividir")
        print("1 . Módulo")

        entrada=int(input("Ingrese la opción: "))
        return entrada

    def ingresar_datos():
        x=int(input("Ingrese el primer numero: "))
        y=int(input("Ingrese el segundo numero: "))
        return x,y
    
    def programa(self):
        opcion=self.menu_principal
        numero1,numero2=self.ingresar_datos
        
    #Crear un objeto de la clase

        miCalculadora = Calculadora()
        miCalculadora.operando_1=numero1
        miCalculadora.operando_2=numero2

        if opcion==1:
            miCalculadora.sumar()
        elif opcion==2:
            miCalculadora.restar()
        elif opcion==3:
            miCalculadora.multiplicar()
        elif opcion==4:
            miCalculadora.dividir()
        elif opcion==5:
            miCalculadora.modulo()

        print(miCalculadora.resultado)

#Crear un nombre de la clase interactuador

miInteractuador=Interactuador()
miInteractuador.programa()





